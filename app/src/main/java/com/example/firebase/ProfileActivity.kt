package com.example.firebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class ProfileActivity : AppCompatActivity() {
    private lateinit var textView : TextView
    private lateinit var buttonlogout : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
        registerListeners()
        textView.text = FirebaseAuth.getInstance().currentUser?.uid
    }
    private fun init(){
        textView = findViewById(R.id.textView)
        buttonlogout = findViewById(R.id.buttonlogout)
    }
    private fun registerListeners(){
        buttonlogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this,SignUpActivity::class.java))
            finish()
        }
    }
}