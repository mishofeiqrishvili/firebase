package com.example.firebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {
    private lateinit var editTextEmailAddress: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextconfirmpassword: EditText
    private lateinit var buttonsubmit: Button
    private lateinit var buttonregister: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()
        registerListeners()
    }

    private fun init() {
        editTextEmailAddress = findViewById(R.id.editTextEmailAddress)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextconfirmpassword = findViewById(R.id.editTextconfirmpassword)
        buttonsubmit = findViewById(R.id.buttonsubmit)
        buttonregister = findViewById(R.id.buttonregister)
    }

    private fun registerListeners() {
        buttonsubmit.setOnClickListener {
            val email = editTextEmailAddress.text.toString()
            val password = editTextPassword.text.toString()
            val confirmpassword = editTextconfirmpassword.text.toString()

            if (email.isNotEmpty() && email.contains("@") && email.contains("mail.com") ||
                email.contains("mail.ru") || email.contains("gmail.com") || email.contains("edu.ge") &&
                email.length >= 11 && password.contentEquals(confirmpassword) && password.length >= 9
            ) {
                Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else {
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            }
        }
        buttonregister.setOnClickListener {
            val email = editTextEmailAddress.text.toString()
            val password = editTextPassword.text.toString()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this, SignUpActivity::class.java))
                    } else {
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }
}


